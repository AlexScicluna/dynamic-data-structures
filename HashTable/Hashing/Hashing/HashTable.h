#pragma once
#include <string>


class HashTable
{
public:
	HashTable();
	~HashTable();

	void Add(std::string key, int a_value);
	int Get(std::string key);


	unsigned int Hash(std::string key);

	int& operator[](const std::string key);

private:
	int* data;
	unsigned int tableSize;
};

 