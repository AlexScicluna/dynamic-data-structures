#include "HashTable.h"



HashTable::HashTable()
{
	data = new int[10];

	tableSize = 10;

}


HashTable::~HashTable()
{
	delete[] data;
}

void HashTable::Add(std::string key, int a_value)
{
	//use the same hash function
	unsigned int index = Hash(key);

	//convert the hash result to a modulas of the tablesize -1 if 10, then 0-9
	index = index % tableSize;

	//return data[index] at value
	data[index] = a_value;
}

int HashTable::Get(std::string key)
{
	//use the same hash function
	unsigned int index = Hash(key);

	//convert the hash result to a modulas of the tablesize -1 if 10, then 0-9
	index = index % tableSize;

	//Return the value at the index
	return data[index];
	
}

//hash = 0 -> 1305333197 (hash 1313)
//hash = 1 -> 1317 (hash 1313)
//hash = 1 -> 1305334509 (hash 1312)
	//1305334509 - 1305333197 = 1312;



unsigned int HashTable::Hash(std::string key)
{
	unsigned int hash = 0;

	for (unsigned int index = 0; index < key.size(); index++)
	{
		hash = (hash * 1313) + data[index];
	}

	return (hash & 0x7FFFFFFF);//0x10000000);
}

int & HashTable::operator[](const std::string key)
{
	// TODO: insert return statement here
	return data[Hash(key) & (tableSize - 1)];
}
