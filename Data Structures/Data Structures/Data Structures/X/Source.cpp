/*
 * - Data Structures
 * - Dynamic Array (int)
 *
 *
*/


#include <iostream>
#include "DynamicArray.h"

void TestDynamicArray();

void main()
{

	std::cout << "\t[-START OF PROGRAM-]" << std::endl;

	TestDynamicArray();

	std::cout << "\t[-END OF PROGRAM-]" << std::endl;
	
	std::cout << std::endl;

	DynamicArray<float> floatArray;
	
	//add values into the array
	for (float i = 0; i < 25; i+=0.5f)
	{
		floatArray.Push(i);
	}

	floatArray.Print();
	floatArray.Shuffle();
	floatArray.SortHighToLow();
	floatArray.Print();

	std::cout << std::endl;
	system("pause");
}//End of main

void TestDynamicArray()
{
	//create dynamic array
	DynamicArray<int> testArray1;
	std::cout << "\n-- Created a blak array (capacity = 10 'default') --" << std::endl;
	
	//add values into the array
	for (int i = 0; i < 25; i++)
	{
		testArray1.Push(i);
	}

	std::cout << "-- Added the values to the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.RemoveFromStart(7);
	std::cout << "-- Removed elements from the [START] of the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.RemoveFromEnd(7);
	std::cout << "-- Removed elements from the [END] of the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.RemoveAt(7);
	std::cout << "-- Removed element at the [INDEX] of the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Push(10);//Adding a duplicate of a value that is already in the array
	
	testArray1.Find(10);
	std::cout << "-- Finding the first value that matches the case --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Find(90);
	std::cout << "-- Finding the first value that matches the case --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Remove(8);
	std::cout << "-- Removing the first match from the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	 //add values into the array
	 for (int i = 0; i < 5; i++)
	 {
	 	testArray1.Push(i);
	 }

	testArray1.AddAt(5,666);
 	std::cout << "-- Adding a value at the index --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Shuffle();
	std::cout << "-- [SHUFFLING] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;


	testArray1.SortLowToHigh();
	std::cout << "-- [SortLowToHigh] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;


	testArray1.Shuffle();
	std::cout << "-- [SHUFFLING] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.SortHighToLow();
	std::cout << "-- [SortHighToLow] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Shuffle();
	std::cout << "-- [SHUFFLING] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Reverse();
	std::cout << "-- [REVERSING] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Clear();
	std::cout << "-- Clearing all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;


}
