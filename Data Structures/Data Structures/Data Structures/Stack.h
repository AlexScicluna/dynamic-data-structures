#pragma once

/*

First in Last out.
Reversed Sequenced based
Menu system that allows to go many layers into the 
menu will need to be closed in reverse.

*/

template<typename T>
class Stack
{
public:
	Stack();
	Stack(unsigned int startingCapacity);
	~Stack();



	//Return true if size == 0;
	bool IsEmpty() const;
	//Gets the used elements
	int GetSize() const;
	//Gets the used elements
	int GetCapacity() const;
	//adds a_value to the top of the stack. 
	// At the end of the array
	void Push(const T a_value);
	//Removes the last element in the array
	int Pop();
	//Returns the last used element in the array
	int Top();

	void Resize(const int a_capacity);


	//Print detials
	void Print()
	{
		
		//keep looking at the next pointer
		for (int index = 0; index < size; index++)
		{
			std::cout << "[" << data[index] << "]" << std::endl;
 
		}

	}
private:
	const int defaultCapacity;
	T* data;
	int capacity;
	int size;
};


template<typename T>
inline Stack<T>::Stack(): defaultCapacity(10)
{
	data = new T[10];

	capacity = 10;

	size = 0;
}

template<typename T>
inline Stack<T>::Stack(unsigned int startingCapacity)
{
	//Create array on the heap
	data = new T[startingCapacity];

	//Setting the capacity
	capacity = startingCapacity;

	//Initilize size
	size = 0;
}


template<typename T>
inline Stack<T>::~Stack()
{
	delete[] data;
}


template<typename T>
bool Stack<T>::IsEmpty() const
{
	//If size is 0 -> Empty
	return (size == 0);
}

template<typename T>
int Stack<T>::GetSize() const
{
	return size;
}

template<typename T>
inline int Stack<T>::GetCapacity() const
{
	return capacity;
}
 

 
template<typename T>
void Stack<T>::Push(const T a_value)
{
	if (size == capacity)
	{
		//resize needed
		capacity *= 1.8f; //Float to an int loss of data, known and intentional
		Resize(capacity);
	}

	data[size] = a_value;

	size++;

}

template<typename T>
int Stack<T>::Pop()
{
	size--;

	return data[size];
}

template<typename T>
int Stack<T>::Top()
{
	return data[size - 1];
}

template<typename T>
inline void Stack<T>::Resize(const int a_capacity)
{
	capacity = a_capacity;


	T* newData = new T[capacity];


	if (size < capacity)
	{
		for (int index = 0; index < size; index++)
		{
			newData[index] = data[index];
		}
	}
	else
	{
		for (int index = 0; index < capacity; index++)
		{
			newData[index] = data[index];
		}
	}

	delete[] data;

	data = newData;

	if (size > capacity)
	{
		size = capacity;
	}


}

 
 