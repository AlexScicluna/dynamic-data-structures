/*
 * - Data Structures
 * - Dynamic Array (int)
 *
 *
*/


#include <iostream>
#include "DynamicArray.h"
#include "LinkedList.h"
#include "Stack.h"
#include "Queue.h"

void TestDynamicArray();
void TestLinkList();
void TestStack();
void TestQueue();


void main()
{
	std::cout << "\t[-START OF PROGRAM-]" << std::endl;
	std::cout << "[----------------Dynamic Array-----------------------]" << std::endl;
	TestDynamicArray();
	std::cout << "[----------------Linked List--------------------]" << std::endl;
	TestLinkList();
	std::cout << "[----------------Stack----------------------]" << std::endl;
	TestStack();
	std::cout << "[----------------Queue----------------------]" << std::endl;
	TestQueue();
	std::cout << "[-------------------------------------------]" << std::endl;
	std::cout << "\t[-END OF PROGRAM-]" << std::endl;

	std::cout << std::endl;
	system("pause");
}//End of main


void TestLinkList()
{
	LinkedList<float> numbers;
	numbers.PushBack(12.3f);
	numbers.PushBack(3332.3f);
	numbers.PushBack(444.4f);
	numbers.PushBack(12.4f);
	numbers.PushBack(3332.42f);
	numbers.PushBack(444.42f);
	numbers.PushBack(444.42f);
	numbers.PushBack(3332.42f);
	numbers.PushBack(12.4f);
	 
	numbers.PushFront(1213123120.23f);

	//numbers.Insert(666.66f, 4);
	numbers.Print();

	std::cout << "Amount of nodes: " << numbers.GetLength() << std::endl;
  	/*numbers.Remove(3);
	numbers.Remove(2);
	numbers.Remove(5);*/ 
	numbers.Remove(1);

	numbers.SortHighToLow();
	numbers.Print();

	std::cout << "Amount of nodes: " << numbers.GetLength() << std::endl;


	std::cout << "Start Node: " << numbers.Begin() << std::endl;
	std::cout << "End Node: " << numbers.End() << std::endl;


	LinkedListNode<float>* foundNode = numbers.Find(444.4f);
	std::cout << "Found Node: " << foundNode->data << std::endl;


	numbers.Shuffle();
	numbers.Print();

}

void TestStack()
{

	Stack<int> stackTest;

	stackTest.Push(24);
	stackTest.Print();
	std::cout << "Size of stack: " << stackTest.GetSize() << std::endl;
	std::cout << "Capacity of stack: " << stackTest.GetCapacity() << std::endl;
	for (int index = 0; index < 20; index++)
	{
		stackTest.Push(index);
	}
	stackTest.Push(35);
	stackTest.Pop();
	stackTest.Print();
	std::cout << "Size of stack: " << stackTest.GetSize() << std::endl;
	std::cout << "Capacity of stack: " << stackTest.GetCapacity() << std::endl;
	std::cout << "Top of the stack: " << stackTest.Top() << std::endl;


}

void TestDynamicArray()
{
	//create dynamic array
	DynamicArray<int> testArray1;
	std::cout << "\n-- Created a blak array (capacity = 10 'default') --" << std::endl;
	
	//add values into the array
	for (int i = 0; i < 25; i++)
	{
		testArray1.Push(i);
	}

	std::cout << "-- Added the values to the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.RemoveFromStart(7);
	std::cout << "-- Removed elements from the [START] of the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.RemoveFromEnd(7);
	std::cout << "-- Removed elements from the [END] of the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.RemoveAt(7);
	std::cout << "-- Removed element at the [INDEX] of the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Push(10);//Adding a duplicate of a value that is already in the array
	
	testArray1.Find(10);
	std::cout << "-- Finding the first value that matches the case --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Find(90);
	std::cout << "-- Finding the first value that matches the case --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Remove(8);
	std::cout << "-- Removing the first match from the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	 //add values into the array
	 for (int i = 0; i < 5; i++)
	 {
	 	testArray1.Push(i);
	 }

	testArray1.AddAt(5,666);
 	std::cout << "-- Adding a value at the index --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.Shuffle();
	std::cout << "-- [SHUFFLING] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;


	testArray1.SortLowToHigh();
	std::cout << "-- [SortLowToHigh] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;


	testArray1.Shuffle();
	std::cout << "-- [SHUFFLING] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.SortHighToLow();
	std::cout << "-- [SortHighToLow] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;


	testArray1.Reverse();
	std::cout << "-- [Reverse] all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	//Subcript test
	std::cout << "-- [SUBSCRIPT OVERLOAD] --\n" << "-|" << testArray1[30] << "|" << std::endl;
	std::cout << "-\n" << std::endl;

	testArray1.Clear();
	std::cout << "-- Clearing all elements in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;

	testArray1.ReplaceAt(4, 89);
	std::cout << "-- [Replacing] a single element in the array --" << std::endl;
	testArray1.Print();
	std::cout << "-\n" << std::endl;
	 
 
}



void TestQueue()
{

	Queue<int> queueTest;

	queueTest.Push(14);
	queueTest.Push(2);
	queueTest.Push(3);
	queueTest.Push(5);
	queueTest.Push(6);
	queueTest.Push(7);

	//queueTest.Pop();
	std::cout << "Size of the stack: " << queueTest.GetSize() << std::endl;

	std::cout << "Popped element: " << queueTest.Pop() << std::endl;
	queueTest.Print();
	std::cout << "Size of the queue: " << queueTest.GetSize() << std::endl;

}