#pragma once
#include <assert.h>
#include <iostream>
#include <ctime>

//#define DEFAULT_CAPACITY 10

template<typename T>
class DynamicArray
{
public:

	//Default Contructor
	DynamicArray();

	//Overloaded Contructor
	DynamicArray(unsigned int startingCapacity);

	//Decontructor
	~DynamicArray();

	//Returns the current elements used in the array
	int GetSize();
	//Returns the current capacity
	int GetCapacity();

	//Replace at the index
	void ReplaceAt(int a_index, T a_value);

	//Resizes the array - user can resize on the fly
	void Resize(unsigned int a_capacity);

	//Add elements to the array
	void Push(T a_value);
	//Add elements to the array
	void AddAt(int a_index, T a_value);

	//Remove element(s) from start
	void RemoveFromStart(T a_value);

	//Remove element(s) from start
	void RemoveFromEnd(T a_value);
	//removes the first value in the array matching the value
	void Remove(T a_value);

	//Remove a single element at index
	void RemoveAt(int index);

	//Find Element
	T Find(T a_value);


	//Randomise the array
	void Shuffle();


	//Bubble Sort
	void SortHighToLow();

	//Bubble Sort
	void SortLowToHigh();

	//Resets all values to NULL (0)
	void Clear();


	//Reverse all elements in the array
	void Reverse();

	//SubScript Overload
	T operator[](const int a_index);

	//Prints the array information
	void Print();


private:

	//
	const int defaultCapacity;

	//contents/data (adress of array of intergers on the heap)
	T* data;

	//Used elements
	unsigned int size;

	//How many elements are available
	unsigned int capacity;

};
//---------------------------------------------------------------------------------

template<typename T>
DynamicArray<T>::DynamicArray() : defaultCapacity(10)
{
	//Create array on the heap
	data = new T[defaultCapacity];

	//Setting the capacity
	capacity = defaultCapacity;

	//Initilize size
	size = 0;

	//Initialise for Shuffle();
	srand(unsigned int(time(NULL)));
}

template<typename T>
DynamicArray<T>::DynamicArray(unsigned int startingCapacity)
{
	//Create array on the heap
	data = new T[startingCapacity];

	//Setting the capacity
	capacity = startingCapacity;

	//Initilize size
	size = 0;
}

template<typename T>
DynamicArray<T>::~DynamicArray()
{
	//Clean up memory
	delete[] data;
}


template<typename T>
int DynamicArray<T>::GetSize()
{
	return size;
}

template<typename T>
int DynamicArray<T>::GetCapacity()
{
	return capacity;
}

template<typename T>
inline void DynamicArray<T>::ReplaceAt(int a_index, T a_value)
{
	data[a_index] = NULL;
	data[a_index] = a_value;
}



template<typename T>
void DynamicArray<T>::Resize(unsigned int a_capacity)
{
	//break if this results to false
	assert(!(a_capacity == 0));

	//Changing capacity
	capacity = a_capacity;

	//Create new array of new capacity size
	T* newData = new T[capacity];

	//if size is less than capacity,
	//copy to size	
	if (size < capacity)
	{
		//Copy each element from old to new
		for (size_t index = 0; index < size; index++)
		{
			newData[index] = data[index];
		}
	}
	//if size is greater than capacity
	//copy to capacity
	else
	{
		//Copy each element from old to new
		for (size_t index = 0; index < capacity; index++)
		{
			newData[index] = data[index];
		}
	}

	//Clean up
	//Setting all elements to NULL
	for (int index = 0; index < GetSize(); index++)
	{
		data[index] = NULL;
	}
	//Deleting the array
	delete[] data;

	//Changing the address of newData to the current array
	data = newData;

	//update size and capacity
	//only update size if capacity is less than the old size (you've lost data)
	if (size > capacity)
	{
		size = capacity;
	}


}


template<typename T>
void DynamicArray<T>::Push(T a_value)
{

	if (size == capacity)
	{
		//Plus 80% of its current capacity
		capacity *= 1.8f;//Float to an unsigned int loss of data, known and intentional
		Resize(capacity);
	}

	//copy a_value into array slot / index "sizes"
	data[size] = a_value;

	//increase size by 1 "index"
	size++;

}

template<typename T>
void DynamicArray<T>::AddAt(int a_index, T a_value)
{

	//Check if there is room
	if (GetSize() == GetCapacity())
	{
		//Make room
		Resize(GetSize() + 1);
	}
	else//There is room
	{
		//Shift elements to the right
		for (int index = GetSize() - 1; index >= a_index; index--)
		{
			//While index is to the right of a_index
			if (index > a_index)
			{
				data[index] = data[index - 1];
			}
		}
	}

	//Clearing the slot for new data coming in
	data[a_index] = NULL;

	//User specified index and adding that value to that position
	data[a_index] = a_value;

}

template<typename T>
void DynamicArray<T>::RemoveFromStart(T a_value)
{
	//Starting from array pos 0 and setting them to null
	for (int index = 0; index < a_value; index++)
	{
		data[index] = NULL;
	}

	//shuffling the valid values towards the start of the array.
	for (int index = 0; index < GetSize(); index++)
	{
		if (index < GetSize() - a_value)
		{
			data[index] = data[index + a_value];
		}
	}

	//Overriding the duplicates at the end of the array
	for (int index = GetCapacity() - 1; index >= (GetSize() - a_value); index--)
	{
		data[index] = NULL;
	}

	//Resize the array to its capacity - a_value
	Resize(GetSize() - a_value);
}

template<typename T>
void DynamicArray<T>::RemoveFromEnd(T a_value)
{
	//Setting the values to NULL from the end of the array back by a_value
	for (int index = GetCapacity() - 1; index >= (GetSize() - a_value); index--)
	{
		data[index] = NULL;
	}

	//Resize the array to its capacity - a_value
	Resize(GetSize() - a_value);
}

template<typename T>
void DynamicArray<T>::Remove(T a_value)
{
	//Find the index of the first match
	int indexOFTarget = Find(a_value);

	//Set the target to NULL
	data[indexOFTarget] = NULL;

	//Shifting the array left by one starting from the indexOFTarget.
	for (int index = indexOFTarget; index < GetSize(); index++)
	{
		data[index] = data[index + 1];
	}

	//Resize the array to its size minus 1 - 'Only removed a single value'
	Resize(GetSize() - 1);

}

template<typename T>
void DynamicArray<T>::RemoveAt(int a_index)
{
	//Set the value at index to NULL
	data[a_index] = NULL;

	//Current index equals the value in the one to the right.
	for (int index = a_index; index < GetSize(); index++)
	{
		data[index] = data[index + 1];
	}

	//Resize the array to its size minus 1 - 'Only removed a single value'
	Resize(GetSize() - 1);
}

template<typename T>
T DynamicArray<T>::Find(T a_value)
{
	for (int index = 0; index < GetSize(); index++)
	{
		if (data[index] == a_value)
		{
			std::cout << "Found it at pos: " << index << std::endl;
			//Return the value that it matched
			return index;
			//Break out of the for loop - Stop searching
			break;
		}
	}

	std::cout << "Found no match" << std::endl;
	//Found nothing.
	return NULL;
}

template<typename T>
void DynamicArray<T>::Shuffle()
{

	for (int index = 0; index < GetSize(); index++)
	{
		//Set Random number
		int randIndex = std::rand() % GetSize();

		//Blank space
		T tempHolder = NULL;

		//Current index into tempHolder
		tempHolder = data[index];
		//Value at randIndex is now assigned to the current index 
		data[index] = data[randIndex];
		//The value at randIndex is now assigned to the tempHolder(original)
		data[randIndex] = tempHolder;

	}

}

template<typename T>
void DynamicArray<T>::SortHighToLow()
{

	int counter = 0;

	for (int index = GetSize(); index >= 0; index--)
	{
		for (int index = GetSize(); index >= 0; index--)
		{

			if (data[index] < data[index + 1])
			{
				//Blank space
				T tempHolder = NULL;

				//Current index into tempHolder
				tempHolder = data[index];
				//Value at index + 1 is now assigned to the current index 
				data[index] = data[index + 1];
				//The value at index + 1 is now assigned to the tempHolder(original)
				data[index + 1] = tempHolder;

			}

			counter++;
		}//End of For

	}//End of For

	std::cout << "[-SWAPS-] : " << counter << std::endl;
}

template<typename T>
void DynamicArray<T>::SortLowToHigh()
{
	int counter = 0;

	for (int index = 0; index < GetSize(); index++)
	{

		for (int index = 0; index < GetSize() - 1; index++)
		{

			if (data[index] > data[index + 1])
			{
				//Blank space
				T tempHolder = NULL;

				//Current index into tempHolder
				tempHolder = data[index];
				//Value at index + 1 is now assigned to the current index 
				data[index] = data[index + 1];
				//The value at index + 1 is now assigned to the tempHolder(original)
				data[index + 1] = tempHolder;

				counter++;
			}

		}//End of For


	}//End of For

	std::cout << "[-SWAPS-] : " << counter << std::endl;
}


template<typename T>
void DynamicArray<T>::Clear()
{
	//Setting the ENTIRE array to NULL
	for (int index = 0; index < GetCapacity(); index++)
	{
		data[index] = NULL;
	}

}

template<typename T>
void DynamicArray<T>::Reverse()
{
	//Create new array with the same capacity size
	int* newData = new int[GetCapacity()];

	int dataIndex = 0;
	//Start with the current size(used elements) and count backwards.
	for (int newDataIndex = GetSize() - 1; newDataIndex >= 0; newDataIndex--)
	{
		//Assigning each element to the newData[] at the current index
		newData[newDataIndex] = data[dataIndex];

		//if not more than size
		if (dataIndex < GetSize())
		{
			//increment on data index
			dataIndex++;
		}

	}

	//Clean up
	//Setting all elements to NULL
	for (int index = 0; index < GetSize(); index++)
	{
		data[index] = NULL;
	}
	//Deleting the array
	delete[] data;

	//Changing the address of newData to the current array
	data = newData;
}

template<typename T>
T DynamicArray<T>::operator[](const int a_index)
{
	return data[a_index];
}


template<typename T>
void DynamicArray<T>::Print()
{
	for (int index = 0; index < GetSize(); index++)
	{
		if (index != 0)
		{
			if (index % 10 == 0)
			{
				std::cout << std::endl;
			}
		}

		std::cout << "-|" << data[index] << "|";
	}
	std::cout << std::endl;
}