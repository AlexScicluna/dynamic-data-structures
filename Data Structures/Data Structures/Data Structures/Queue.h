#pragma once
 
/*

First in First out
Sequence based
A list of tasks to be done 
in order as they enter the Queue.

*/


template<typename T>
class Queue
{
public:
	Queue();
	Queue(unsigned int startingCapacity);
	~Queue();



	//Return true if size == 0;
	bool IsEmpty() const;
	//Gets the used elements
	int GetSize() const;
	//adds a_value to the top of the Queue. 
	// At the end of the array
	void Push(const T a_value);
	//Removes the first element in the array
	T Pop();
	//Returns the first used element in the array
	int Front();
	//Returns the last used element in the array
	int Back();

	//Print detials
	void Print()
	{

		//keep looking at the next pointer
		for (int index = 0; index < size; index++)
		{
			std::cout << "[" << data[index] << "]" << std::endl;

		}

	}
private:
	const int defaultCapacity;
	T* data;
	int capacity;
	int size;
};


template<typename T>
inline Queue<T>::Queue() : defaultCapacity(10)
{
	data = new T[10];

	capacity = 10;

	size = 0;
}

template<typename T>
inline Queue<T>::Queue(unsigned int startingCapacity)
{
	//Create array on the heap
	data = new T[startingCapacity];

	//Setting the capacity
	capacity = startingCapacity;

	//Initilize size
	size = 0;
}


template<typename T>
inline Queue<T>::~Queue()
{
	delete[] data;
}


template<typename T>
bool Queue<T>::IsEmpty() const
{
	//If size is 0 -> Empty
	return (size == 0);
}

template<typename T>
int Queue<T>::GetSize() const
{
	return size;
}



template<typename T>
void Queue<T>::Push(const T a_value)
{
	if (size == capacity)
	{
		//resize needed
		//capacity *= 1.8f;
		//Resize(new capacity size);
	}

	data[size] = a_value;

	size++;

}

template<typename T>
inline T Queue<T>::Pop()
{
	T tempHolder = data[0];
	//Shift Left
 

	for (int index = 0; index < size; index++)
	{
		data[index] = data[index + 1];
	}

	size--;

	return tempHolder;
}

template<typename T>
inline int Queue<T>::Front()
{
	return data[0];
}

template<typename T>
inline int Queue<T>::Back()
{
	return data[size - 1];
}
