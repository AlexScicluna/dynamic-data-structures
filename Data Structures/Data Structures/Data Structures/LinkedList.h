#pragma once
#include <iostream>
#include <ctime>

template <typename T>
class LinkedListNode;

template <typename T>
class LinkedList
{
public:
	LinkedList()
	{
		start = nullptr;
		count = 0;
		//Initialise for Shuffle();
		srand(unsigned int(time(NULL)));
	}



	~LinkedList()
	{
		//delete node list
		delete start;
	}


	//Pointer to the first node

	//Add
	void PushBack(T a_value)
	{
		//true then first node
		if (start == nullptr)
		{
			//Create Linked Lisk Node
			//Store value inside start node
			//Setup next and prev Pointers

			//New node on the heap
			LinkedListNode<T>* newNode = new LinkedListNode<T>();

			//Node data
			newNode->data = a_value;

			newNode->next = nullptr;
			newNode->prev = nullptr;


			start = newNode;

			//Keeping track of the amount of nodes
			count++;
		}
		else//Already a start node, create next node
		{
			//New node on the heap
			LinkedListNode<T>* newNode = new LinkedListNode<T>();
			//Node data
			newNode->data = a_value;

			newNode->next = nullptr;//Its the final node

									//find the final node
			LinkedListNode<T>* currentNode = start;

			//Searching for the end node
			while (currentNode->next != nullptr)
			{
				//Point to the next node in the LinkedList
				currentNode = currentNode->next;
			}
			//found end node

			//Add new node to the end node
			currentNode->next = newNode;
			//setting the newNode to the previous
			newNode->prev = currentNode;

			//Keeping track of the amount of nodes
			count++;

		}

	}

	void PushFront(T a_value)
	{

		//Should always return true. 
		if (start->prev == nullptr)
		{
			//Create new node
			LinkedListNode<T>* newNode = new LinkedListNode<T>();

			//Add value to the newNode
			newNode->data = a_value;

			//Connect the newNode to the current Start
			newNode->next = start;
			//Set the newNode previous to nullptr
			newNode->prev = nullptr;

			//
			start->prev = newNode;

			//Move the start* to the new node
			start = newNode;

			//Add on count
			count++;

		}



	}

	//Adds a newNode at index
	void Insert(T a_value, unsigned int a_index)
	{
		if (a_index == 0)
		{
			PushFront(a_value);
			return;
		}

		int indexOfNode = 0;

		LinkedListNode<T>* newNode = new LinkedListNode<T>();
		newNode->data = a_value;
		newNode->next = nullptr;
		newNode->prev = nullptr;

		LinkedListNode<T>* currentNode = start;

		while (currentNode->next != nullptr)
		{
			indexOfNode++;


			if (indexOfNode == a_index + 1)
			{
				newNode->next = currentNode;
				newNode->prev = currentNode->prev;
				currentNode->prev->next = newNode;
				currentNode->prev = newNode;

				return;

			}

			currentNode = currentNode->next;

		}



	}

	//Removes the first node
	void PopFront()
	{
		LinkedListNode<T>* currentNode = start;

		start = currentNode->next;

		currentNode->next->prev = nullptr;

		count--;

		delete currentNode;
 
	}
	
	//Removes the last node
	void PopBack()
	{
		LinkedListNode<T>* currentNode = start;
		//Find the end node
		while (currentNode->next != nullptr)
		{
			currentNode = currentNode->next;
		}

		currentNode->prev->next = nullptr;

		count--;
		delete currentNode;

		
	}

	//Removes at index
	void Remove(unsigned int a_index)
	{
		LinkedListNode<T>* currentNode = start;

		if (a_index == 1)
		{
			PopFront();
			return;
		}

		if (a_index == count)
		{
			PopBack();
			return;
		}


		int indexOfNode = 0;
		
		while (currentNode->next != nullptr)
		{
			indexOfNode++;
		
			if (indexOfNode == a_index)
			{
				break;
			}

			currentNode = currentNode->next;
		}

		currentNode->prev->next = currentNode->next;
		currentNode->next->prev = currentNode->prev;
		delete currentNode;
		count--;

	}

	//Returns the first node
	LinkedListNode<T>* Begin();
	//Returns the end node
	LinkedListNode<T>* End();
	
	//Find value
	LinkedListNode<T>* Find(T a_value);

	//Bubble Sort
	void SortHighToLow()
	{
		LinkedListNode<T>* currentNode = start;

		int loops = 0;

		bool swapNeeded = true;
		while (swapNeeded == true)
		{
			swapNeeded = false;

			while (currentNode->next != nullptr)
			{
				if (currentNode->data < currentNode->next->data)
				{
					//Blank container
					LinkedListNode<T>* tempHolder = new LinkedListNode<T>();

					//CurrentNode goes into the temp
					tempHolder->data = currentNode->data;
					//The one to the right goes left
					currentNode->data = currentNode->next->data;
					//next = original
					currentNode->next->data = tempHolder->data;

					loops++;
					swapNeeded = true;
				}
 
				//Point to the next node in the LinkedList
				currentNode = currentNode->next;


			}//while

			//reset the current node to the start of the node list
			currentNode = start;
		}//outer while
		
 
		std::cout << "[-SWAPS-] : " << loops << std::endl;

	}//end of func


	void Shuffle()
	{
		LinkedListNode<T>* currentNode = start;

		LinkedListNode<T>* newNode = new LinkedListNode<T>();

		for (int index = 0; index < GetLength(); index++)
		{

			while (currentNode->next != nullptr)
			{

				//Set Random number
				int randIndex = std::rand() % GetLength();

				if (index != randIndex)
				{
					//Blank container
					T tempHolder = NULL;
				
					//CurrentNode goes into the temp
					tempHolder = currentNode->data;
					//The one to the right goes left
					currentNode->data = newNode->data;
					//next = original
					newNode->data = tempHolder;
 
				}

				//Point to the next node in the LinkedList
				currentNode = currentNode->next;

			}//while
			currentNode = start;
		}
	}



	//Print detials
	void Print()
	{
		//start at start
		LinkedListNode<T>* currentNodeAddress = start;

		//keep looking at the next pointer
		while (currentNodeAddress != nullptr)
		{
			std::cout << "[" << currentNodeAddress->data << "]" << std::endl;

			//now look at the next node
			currentNodeAddress = currentNodeAddress->next;
		}

	}

	LinkedListNode<T>* start;
	int count;//size of the linked list - how many nodes
	int GetLength();
};

template <typename T>
class LinkedListNode 
{
public:	

	LinkedListNode()
	{
		data = T();
		next = nullptr;
		prev = nullptr;
	}

	LinkedListNode(T a_data)
	{
		data = a_data;
		next = nullptr;
		prev = nullptr;
	}

 	T data;
	LinkedListNode<T>* next;
	LinkedListNode<T>* prev;


};
 

template<typename T>
inline LinkedListNode<T>* LinkedList<T>::Begin()
{
	LinkedListNode<T>* currentNode = start;
	return currentNode;
}

template<typename T>
inline LinkedListNode<T>* LinkedList<T>::End()
{
	LinkedListNode<T>* currentNode = start;
	//Find the end node
	while (currentNode->next != nullptr)
	{
		currentNode = currentNode->next;
	}
	return currentNode;
}

template<typename T>
inline LinkedListNode<T>* LinkedList<T>::Find(T a_value)
{
	LinkedListNode<T>* currentNode = start;
	//Find the end node
	while (currentNode->next != nullptr)
	{
		if (currentNode->data == a_value)
		{
			return currentNode;
		}
		currentNode = currentNode->next;
	}
	return nullptr;
}

template<typename T>
inline int LinkedList<T>::GetLength()
{
	return count;
}
