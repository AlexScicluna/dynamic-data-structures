#pragma once
#include "BinaryTreeNode.h"

 
class BinaryTree
{
public:
	BinaryTree();
	~BinaryTree();

	void Insert(int a_data);
	void Remove(int a_data);
	void PrintNodes();

private:
	
	void RemoveLeaf(BinaryTreeNode* nodeToRemove, BinaryTreeNode* nodeToRemovesParent);
	void RemoveOneChild(BinaryTreeNode* nodeToRemove, BinaryTreeNode* nodeToRemovesParent);
	void RemoveTwoChild(BinaryTreeNode* currentNode, BinaryTreeNode* nodeToRemovesParent);

	//Root Node
	BinaryTreeNode* root;
	
};



 