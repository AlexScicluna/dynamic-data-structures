#include "BinaryTree.h"
#include <iostream>
#include <ctime>


void main()
{
	srand(unsigned int(time(NULL)));
	BinaryTree numberTree;

	std::cout << "Adding 7 numbers." << std::endl;
	//numberTree.Insert(5);
	//numberTree.Insert(2);
	//numberTree.Insert(6);
	//numberTree.Insert(3);
	//numberTree.Insert(1);
	//numberTree.Insert(4);
	//numberTree.Insert(7);

	for (size_t index = 0 + 1; index <= 25; index++)
	{
		numberTree.Insert((std::rand() % 25) + 1);
	}
		
  

	std::cout << "\nPrinting the tree" << std::endl;
	numberTree.PrintNodes();

	int numberRemoving = (std::rand() % 25) + 1;
	std::cout << "\n\nRemoving a node: " << numberRemoving << std::endl;
	numberTree.Remove(numberRemoving);
	

	std::cout << "\nPrinting the tree" << std::endl;
	numberTree.PrintNodes();
	
	std::cout << "\n" << std::endl;
	system("pause");
}