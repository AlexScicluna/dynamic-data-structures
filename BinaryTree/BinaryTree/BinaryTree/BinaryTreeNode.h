#pragma once


class BinaryTreeNode
{
public:
	BinaryTreeNode();
	~BinaryTreeNode();

	BinaryTreeNode(int a_data);
	int GetData() const;
	void PrintPreOrder();

	int data;
	BinaryTreeNode* left_Child;
	BinaryTreeNode* right_Child;

};
