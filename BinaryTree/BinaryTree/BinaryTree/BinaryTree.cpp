#include "BinaryTree.h"
#include <iostream>

//Default constructor for the Binary Tree
BinaryTree::BinaryTree()
{
	//no root node, empty tree
	root = nullptr;
}

BinaryTree::~BinaryTree()
{
}

void BinaryTree::Insert(int a_data)
{
	BinaryTreeNode* newNode = new BinaryTreeNode();
	//Generate a new node with data
	newNode->data = a_data;
	newNode->left_Child = nullptr;
	newNode->right_Child = nullptr;

	//if no root node, set it as the root node
	if (root == nullptr)
	{
		root = newNode;
		return;
		//root->data = newNode->data;
		//root->left_Child = newNode->left_Child;
		//root->right_Child = newNode->right_Child;
	}
 

	//Create a current node ppointer
	BinaryTreeNode* currentNode = root;

	while (true)
	{
		//New node is less than current node
		if (newNode->data < currentNode->data)
		{
			//Move the currentNode ptr to the left child of the currentNode
			if (currentNode->left_Child != nullptr)
			{
				currentNode = currentNode->left_Child;
			}
			//Is nullptr than add the newNode->data to the currentNode's data
			else
			{
				//Setting the currentNodes data to the newNode's data
				currentNode->left_Child = newNode;
				currentNode->left_Child->data = newNode->data;
				//Exit out of the while loop
				break;
			}
		}
		//If the newNode is more or equal to the currentNode->data
		else if (newNode->data > currentNode->data)
		{
			//Move the currentNode ptr to the right child of the currentNode
			if (currentNode->right_Child != nullptr)
			{
				currentNode = currentNode->right_Child;
			}
			//Is nullptr than add the newNode->data to the currentNode's data
			else
			{
				//Setting the currentNodes data to the newNode's data
				currentNode->right_Child = newNode;
				currentNode->right_Child->data = newNode->data;
				//Exit out of the while loop
				break;
			}
		}
		//Node was equal to the current node
		else
		{
			currentNode->data = newNode->data;
			break;
		}

	}//End of while
 
}

void BinaryTree::Remove(int a_data)
{
	
	//find the node to remove!!
	BinaryTreeNode* nodeToRemove = root; 
	BinaryTreeNode* nodeToRemovesParent = nullptr;

	//Searching for a match
	while (true)
	{
		if (nodeToRemove->data == a_data)
		{
			//Found the node
			//nodeToRemovesParent = nodeToRemove;
			break;
		}

		//If a_data is less than
		else if (a_data < nodeToRemove->data)
		{
			//Set the parent to the 'currentNode'
			nodeToRemovesParent = nodeToRemove;
			//if nodeToRemove->left_Child was nullptr
			if (nodeToRemove->left_Child != nullptr)
			{
				//move the current node
				nodeToRemove = nodeToRemove->left_Child;
			}
			else
			{
				//a_data not found where it should be in the tree
				break;
			}	

		}
		else
		{
			//Set the parent to the 'currentNode'
			nodeToRemovesParent = nodeToRemove;
			//move the current node
			nodeToRemove = nodeToRemove->right_Child;
	
			//if nodeToRemove->right_Child was nullptr
			if (nodeToRemove->right_Child == nullptr)
			{
				//a_data not found where it should be in the tree
				break;
			}
		}
	
	}//End of while


 //if nodeToRemove has 0 children
	if (nodeToRemove->left_Child == nullptr && nodeToRemove->right_Child == nullptr)
	{
		//Was a leaf node
		RemoveLeaf(nodeToRemove, nodeToRemovesParent);
	}

//else if two children
	else if (nodeToRemove->left_Child != nullptr && nodeToRemove->right_Child != nullptr)
	{
		//Both children have a value 
		RemoveTwoChild(nodeToRemove, nodeToRemovesParent);
	}
	else
	{
		//Not both null or have a value, so must be one and not the other
		RemoveOneChild(nodeToRemove, nodeToRemovesParent);
	}



}

void BinaryTree::PrintNodes()
{
	if (root != nullptr)
	{
		root->PrintPreOrder();
	}
}

void BinaryTree::RemoveLeaf(BinaryTreeNode * nodeToRemove, BinaryTreeNode * nodeToRemovesParent)
{
	std::cout << "\nLeaf node" << std::endl;
	if (nodeToRemovesParent->left_Child == nodeToRemove)
	{
		nodeToRemovesParent->left_Child = nullptr;
	}
	else
	{
		nodeToRemovesParent->right_Child = nullptr;
	}

	//Was a leaf node
	delete nodeToRemove;
}

void BinaryTree::RemoveOneChild(BinaryTreeNode * nodeToRemove, BinaryTreeNode * nodeToRemovesParent)
{
	std::cout << "\nNode with 1 Child" << std::endl;
	//find the child of the node to remove
	BinaryTreeNode* nodeToRemovesChild = nullptr;

	if (nodeToRemove->left_Child == nullptr)
	{
		nodeToRemovesChild = nodeToRemove->right_Child;
	}
	else
	{
		nodeToRemovesChild = nodeToRemove->left_Child;
	}


	//check which side the nodeToRemove is on
	if (nodeToRemovesParent->left_Child == nodeToRemove)
	{
		//it's on left of parent
		//set parent->leftChild = nodeToRemove's only child
		nodeToRemovesParent->left_Child = nodeToRemovesChild;
	}
	else
	{
		//it's on right of parent
		//set parent->rightChild = nodeToRemove's only child
		nodeToRemovesParent->right_Child = nodeToRemovesChild;
	}


	delete nodeToRemove;

}

void BinaryTree::RemoveTwoChild(BinaryTreeNode * currentNode, BinaryTreeNode * nodeToRemovesParent)
{
	std::cout << "\nNode with 2 Children" << std::endl;
	//Holder for the nodeFound 
	BinaryTreeNode* nodeFound = currentNode;

	//Push the nodeToRemove to teh right once
	currentNode = currentNode->right_Child;

	//Find the most left node that isn't a nullptr
	while (currentNode->left_Child != nullptr)
	{
		currentNode = currentNode->left_Child;
	}
	//Found most left node that is more than the nodeFound

	
	//Swap the 'currentNode' most left node 'overriding' the nodeFound
	nodeFound->data = currentNode->data;

	
	//remove currentNode - duplicate / old (child of the one we want to remove)
	
	//Find the side that the child node is on and move that to the node found right_child
	
	//if nodeToRemove has a right_Child
	if (currentNode->right_Child != nullptr)
	{
		std::cout << "\nNode with 1 Child to the right" << std::endl;
		//Not both null or have a value, so must be one and not the other
		RemoveOneChild(currentNode, nodeFound);
	}
	else
	{
		//Was have a leaf node
		RemoveLeaf(currentNode, nodeFound);
	}


}
