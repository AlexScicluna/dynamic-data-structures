#include "BinaryTreeNode.h"
#include <iostream>


BinaryTreeNode::BinaryTreeNode()
{
	//data of the node equals the data parsed into it
	data = 0;
	//Set the nodes Children to nullptr
	left_Child = nullptr;
	right_Child = nullptr;
}


BinaryTreeNode::~BinaryTreeNode()
{
}


//Overloaded Constructor a new Binary Tree Node 
BinaryTreeNode::BinaryTreeNode(int a_data)
{
	//data of the node equals the data parsed into it
	data = a_data;
	//Set the nodes Children to nullptr
	left_Child = nullptr;
	right_Child = nullptr;
}

//Returns the data of the node that calls it
int BinaryTreeNode::GetData() const
{
	return data;
}

void BinaryTreeNode::PrintPreOrder()
{
	std::cout << "[" << data << "] ";

	//Print Left Child
	if (left_Child != nullptr)
	{
		left_Child->PrintPreOrder();
	}


	//Print Right Child
	if (right_Child != nullptr)
	{
		right_Child->PrintPreOrder();
	}

}
